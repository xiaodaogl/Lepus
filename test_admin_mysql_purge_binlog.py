#!//bin/env python
# coding:utf-8

import pymysql
from admin_mysql_purge_binlog import admin_mysql_purge_binlog
import unittest


class TestAdminMysqlPurgeBinlog(unittest.TestCase):
    def test_admin_mysql_purge_binlog(self):
        admin_mysql_purge_binlog(host='127.0.0.1',
                                 port=3306,
                                 passwd='test123',
                                 user='root',
                                 binlog_store_days=7)


if __name__=='__main__':
    unittest.main()