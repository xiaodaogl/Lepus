#!/bin/env python
# -*-coding:utf-8-*-

import unittest
import pymysql
from check_mysql import check_mysql


class TestCheckMysql(unittest.TestCase):
    def setUp(self):
        self.conn = pymysql.connect(host='127.0.0.1',
                                    port=3306,
                                    user='root',
                                    passwd='test123')

    def test_check_mysql(self):
        check_mysql(host='127.0.0.1',
                    port='3306',
                    username='root',
                    password='test123',
                    server_id='1',
                    tags='test')


if __name__ == '__main__':
    unittest.main()
